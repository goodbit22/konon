#!/usr/bin/env bats

@test "inserting available address and port" {
    source functions.sh
    run  nc -4 -l 1234 &
    run  port_available "127.0.0.1" "1234"
    [ "$status" -eq 0 ]
}

@test "inserting valid port" {
    source functions.sh
    run  nc -4 -l 1234 &
    run  port_available "127.0.0.1" "10000" 
    [ "$status" -eq 0 ]
}

@test "inserting valid address but the port is correct" {
    run nc -w 1 127.0.0.1 1234 
    source functions.sh
    run  nc -4 -l 1235 &
    run  port_available "254.0.0.1" "1235" 
    [ "$status" -eq 0 ] 
}

@test "inserting correct ipv4 address" {
    run nc -w 1 127.0.0.1 1235 
    source functions.sh
    run test_address "127.0.0.1"  
    [ "$status" -eq 0  ]
}

@test "inserting invalid ipv4 address " {
    source functions.sh
    run test_address "127.0.0.1" 
    [ ! "$status" -eq 0  ]

}