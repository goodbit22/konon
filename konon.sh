#!/bin/bash

# shellcheck source=/dev/null
declare -ra load_files=("functions.sh" "dictionary.sh" "brute.sh")
for load_file in "${load_files[@]}"
do
    if [ -f "./${load_file}" ];then
        source "./${load_file}"
    else
        echo -e "${RED}${load_file} file cannot be found${WHITE}"
  	    exit 1;   
    fi
done 

hashcat_usage(){
    if [ "$1" = "brute" ];then
        hashcat_usage_brute   
    else 
        hashcat_usage_dictionary "$2"
    fi
}

unshadow_usage(){
    read -rp "Enter the location of the passwd file: " passwd
    read -rp "enter the location of the shadow file: " shadow
    date="$(date +"%m:%d:%y_%H:%M.txt")"
    unshadow "$passwd" "$shadow" > "crack${date}"
    [ "$?" -eq 1 ] && echo "${RED}The file path entered is invalid${WHITE}"

}

windows_password(){
    read -rp "Enter the location of the sam file: " sam
    chntpw -l "$sam"
    [ "$?" -eq 1 ] && echo "${RED}The file path entered is invalid${WHITE}"
    read -rp "Enter the location of the hash file with the passwords: " hash_window
    john --format=LM "$hash_window"       
    [ "$?" -eq 1 ] &&  echo "${RED}The file path entered is invalid${WHITE}"
}

linux_password(){
    read -rp "Enter the location of the shadow file: " shadow
    john "$shadow"
    [ "$?" -eq 1 ] && echo "${RED}The file path entered is invalid${WHITE}"
} 

john_usage(){
    local opts=("Unix password" "Windows password" "Back")
    PS3="Select option: "
    select option in "${opts[@]}"                                                                            
    do                                                                                                          
    case $option in                                                                                           
        "${opts[0]}") 
            echo "${CYAN}you have selected the option: $REPLY ${WHITE}"
            linux_password
            break                                                                                                                                                                                                                                                                  
            ;;                                                                                        
        "${opts[1]}")                                                                                  
            echo "${CYAN}you have selected the option: $REPLY ${WHITE}"
            windows_password                                                                                             
            break
            ;;
        "${opts[2]}")                                                                                  
            echo "${CYAN}you have selected the option: $REPLY ${WHITE}"                                                                                               
            break
            ;;
        *) echo -e "${RED}Invalid option: $REPLY ${WHITE}" ;;         
    esac                                                                                                      
    done                         
}
                                                                                                                      
hashid_usage(){
    read -rp "Enter hash: " hash1
    hashid "$hash1"
}

smbclient_usage(){
    local status=1
    while [ "$status"  -eq  1 ]; 
    do
        read -rp "Enter address ipv4: " host
        test_address "$host"
        status="$?"
        [ "$status" -eq 1 ] && echo "${YELLOW}The ipv4 address entered  is invalid ${WHITE}"
    done
    ping -c 2 "$host"  &> '/dev/null' 
    status="$?"
    if [ "$status" -eq 0 ];then
        snmpwalk -c public "$host"
        read -rp "Enter the user into smb: " login_smb
        read -rp "Enter the password into smb: " password_smb
        enum4linux -u "$login_smb" -p "$password_smb"  -U -M -S -d -G "$host"        
        [ "$?" -eq 1 ] && echo "data entered  is valid incorrect"
    else
        echo "ipv4 address entered is not available"
    fi
}        

rpcclient_usage(){
    local status=1
    while [ "$status"  -eq  1 ]; 
    do
        read -rp "Enter address ipv4: " host
        test_address "$host"
        status="$?"
        [ "$status" -eq 1 ] &&  echo "${YELLOW}ipv4 address entered  is valid ${WHITE}"
    done
    ping -c 2 "$host"  &> '/dev/null'
    status="$?" 
    if [ "$status" -eq 0 ];then
        read -rp "Enter the login: " login
        read -rp "Enter the password: " password
        terminator -new-tab --command "rpcclient -U ${login}%${password} $host" &
    else
        echo "ipv4 address entered is not available"
    fi
}

main_brute(){
    create_directory
    view_banner
    PS3="Select options: "
    declare -ra options=("Usage of rcpclient" "Usage of snmpwalk & enum4linux" "Usage of hashid" "Usage of john" "Usage of unshadow" 
    "Usage of hashcat" "Exit")
    COLUMNS=10
    select option in "${options[@]}"                                                                            
    do                                                                                                          
     case $option in                                                                                                                                                
             "${options[0]}")                                                                                  
                    echo "${CYAN}Select option $REPLY ${WHITE}"
                    rpcclient_usage                                                                                                                             
                    menu_main "brute"
                    ;;       
             "${options[1]}")                                                                                  
                    echo "${CYAN}Select option  $REPLY ${WHITE}"
                    smbclient_usage                                                                                                                              
                    menu_main "brute"
                    ;;     
            "${options[2]}")                                                                                  
                    echo "${CYAN}Select option  $REPLY ${WHITE}"
                    hashid_usage                                                                                                                         
                    menu_main "brute"
                    ;;
            "${options[3]}")                                                                                  
                    echo "${CYAN}Select option  $REPLY ${WHITE}"
                    john_usage                                                                                                                        
                    menu_main "brute"
                    ;;
            "${options[4]}")                                                                                  
                    echo "${CYAN}Select option  $REPLY ${WHITE}"
                    unshadow_usage                                                                                                                         
                    menu_main "brute"
                    ;;   
             "${options[5]}")                                                                                  
                    echo "${CYAN}Select option  $REPLY ${WHITE}"
                    hashcat_usage  "$logins_file"                                                                                                                       
                    menu_main "brute"
                    ;;   
            "${options[6]}")                                                                                  
                    echo "end"
                    break
                    ;;                                                                                                      
             *) 
                    clear
                  	COLUMNS=10
                    echo -e "${RED}Invalid option: $REPLY ${WHITE}"
                    menu_main "brute"
                    ;;                                                            
     esac                                                                                                      
    done
}

main_dictionary(){
    logins_file="$1"
    passwords_file="$2"
    create_directory
    view_banner
    PS3="Select options: "
    options=("Attack by hydra" "Attack by ncrack" "Usage of rcpclient" "Usage of snmpwalk & enum4linux" "Usage of hashid" "Usage of john" "Usage of unshadow" 
    "Usage of hashcat" "Exit")
    COLUMNS=12
    select option in "${options[@]}"                                                                            
    do                                                                                                          
     case $option in                                                                                           
            "${options[0]}") 
                    echo "${CYAN}Select option $REPLY ${WHITE}"
                    hydra_dictionary_attack "$logins_file" "$passwords_file"
                    menu_main  "dict"                                                                                                                                                                                                                                                              
                    ;;                                                                                        
             "${options[1]}")                                                                                  
                    echo "${CYAN}Select option  $REPLY ${WHITE}"
                    ncrack_dictionary_attack "$logins_file" "$passwords_file"
                    menu_main "dict"
                    ;;                                                                                        
             "${options[2]}")                                                                                  
                    echo "${CYAN}Select option $REPLY ${WHITE}"
                    rpcclient_usage                                                                                                                             
                    menu_main "dict"
                    ;;       
             "${options[3]}")                                                                                  
                    echo "${CYAN}Select option  $REPLY ${WHITE}"
                    smbclient_usage                                                                                                                              
                    menu_main "dict"
                    ;;     
            "${options[4]}")                                                                                  
                    echo "${CYAN}Select option  $REPLY ${WHITE}"
                    hashid_usage                                                                                                                         
                    menu_main "dict"
                    ;;
            "${options[5]}")                                                                                  
                    echo "${CYAN}Select option  $REPLY ${WHITE}"
                    john_usage                                                                                                                        
                    menu_main "dict"
                    ;;
            "${options[6]}")                                                                                  
                    echo "${CYAN}Select option  $REPLY ${WHITE}"
                    unshadow_usage                                                                                                                         
                    menu_main "dict"
                    ;;   
             "${options[7]}")                                                                                  
                    echo "${CYAN}Select option  $REPLY ${WHITE}"
                    hashcat_usage  "$logins_file"                                                                                                                       
                    menu_main "dict"
                    ;;   
            "${options[8]}")                                                                                  
                    echo "end"
                    break
                    ;;                                                                                                      
             *) 
                    clear
                    COLUMNS=12
                    echo -e "${RED}Invalid option: $REPLY ${WHITE}"
                    menu_main
                    ;;                                                            
     esac                                                                                                      
    done
} 
    while getopts "l:p:" parameters; do
        case "${parameters}" in
        l)
            if [ "${1}" != '-l' ] || [ "$2" == "" ];then
                usage; exit 1
            fi
            logins_file=${OPTARG}
            ;;
        p)
            if [ "${1}" != '-l' ] || [ "$3" != "-p" ] || [ "$4" == "" ];then
                usage; exit 1
            fi
            passwords_file=${OPTARG}
            ;;
        \?)
            usage
            exit 1
            ;;    
        *)
            echo "nothing"
            ;;
        esac
    done
    if [  -n "$logins_file" ] && [  -n "$passwords_file" ];then
        if [  -f "$logins_file" ] && [ -f "$passwords_file" ];then        
            if [ "${logins_file##*.}" == "txt" ] && [ "${passwords_file##*.}" == "txt" ]; then
                main_dictionary "$logins_file" "$passwords_file"
            else
                echo "The $logins_file or $passwords_file Nie poprawne rozszerzenie plikow"
            fi
        else
            echo "The $logins_file or $passwords_file Nie ma takiego pliku "
        fi 
    elif  [  -n "$logins_file"  ]  && [ -z "$passwords_file"  ];then
        if [  -f "$logins_file" ];then
            if [ "${logins_file##*.}" == "txt" ]; then
                main_dictionary "$logins_file" "$logins_file" 
            else
                echo "The $logins_file Nie poprawne rozszerzenie plikow"
            fi
        else
            echo "The $logins_file Nie takich plikow"
        fi
    else
        main_brute
    fi   